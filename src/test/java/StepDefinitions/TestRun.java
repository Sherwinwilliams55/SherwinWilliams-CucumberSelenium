package StepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tests.ExpediaElements;

public class TestRun {
	public  WebDriver driver;
	public WebElement element;
	String baseUrl = "http://www.expedia.com/";
	
@Before
	public void setup(){
		System.setProperty("webdriver.gecko.driver", "/Users/SherwinWilliams/Projects/Selenium Files/geckodriver");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(01, TimeUnit.SECONDS);
		
	}
	

	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable{
		driver.get(baseUrl);
	}

	@When("^User enters Username and Password$")
	public void user_enters_Username_and_Password() throws Throwable  {
		ExpediaElements.clickAccount(driver).click();

		ExpediaElements.signIn(driver).click();

		ExpediaElements.emailInput(driver).sendKeys("sherwinwilliams55@gmail.com");

		ExpediaElements.psswrdInput(driver).sendKeys("danny1234");
	}

	@When("^Clicks Sign In$")
	public void clicks_go_button() throws Throwable {
		ExpediaElements.submitBttn(driver).click();
	}

	@Then("^He/She is on Welcome Page$")
	public void he_She_is_on_Welcome_Page() throws Throwable {
		Thread.sleep(2000);
		driver.quit();

	}

}
