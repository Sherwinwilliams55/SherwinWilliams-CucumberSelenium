package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ExpediaElements {
	public  static WebElement element = null;
	public static WebDriver driver;
	


	/*
	 * Account Tag
	 */
	public static WebElement clickAccount(WebDriver driver) {
		element = driver.findElement(By.id("header-account-menu"));
		return element;
	}

	/*
	 * Sign in Tag
	 */
	public static  WebElement signIn(WebDriver driver) {
		element = driver.findElement(By.xpath(".//*[@id='account-signin']"));
		return element;
	}

	/*
	 * Email Input
	 */
	public static WebElement emailInput(WebDriver driver) {
		element = driver.findElement(By.xpath(".//*[@id='signin-loginid']"));
		return element;
	}

	/*
	 * Password Input
	 */
	public static  WebElement psswrdInput(WebDriver driver) {
		element = driver.findElement(By.id("signin-password"));
		return element;
	}
	
	/*
	 * Submit Button
	 */
	public static WebElement submitBttn(WebDriver driver) {
		element = driver.findElement(By.id("submitButton"));
		return element;
	}
	

}
